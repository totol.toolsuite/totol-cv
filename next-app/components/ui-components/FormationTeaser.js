"use client"

import { renderRichText } from "@storyblok/react";
import ContentPage from "./ContentPage";
import { useInView } from "framer-motion"
import { useRef } from "react";
import { motion } from 'framer-motion'
import { createBreakpoint } from "react-use";
const ProjectTeaser = ({ title, image, school, skills, dateStart, dateEnd }) => {

  
  const useBreakpoint = createBreakpoint({ small: 400, medium: 600 });

  const isMedium = useBreakpoint() === 'medium'

  const richText = { __html: renderRichText(skills) };

  const ref = useRef(null)
  const isInView = useInView(ref)

  const titleVariants = {
    visible: { y: 0 },
    before: { y: 100 }
  }

  return (
    <ContentPage >
      <div className="max-h-[25%]" ref={ref}>
        <div className="overflow-hidden ">
          <div className="float-left md:absolute neon p-4 top-4 left-4 md:top-12 md:left-36 text-left font-bold md:text-xl md:neon">{`${new Date(dateStart).getFullYear()} • ${new Date(dateEnd).getFullYear()}`}</div>
          <motion.h1 
            className="text-3xl md:text-5xl ml-4 text-center"
            variants={titleVariants}
            animate={isInView ? 'visible' : 'before'}
            transition={{ duration: 0.7 }}
          >
            {school}
          </motion.h1>
        </div>
        
        <div className="italic"><div>{title}</div></div>
      </div>
      <div className={`grid md:grid-cols-[7fr_3fr] items-center justify-center gap-2 md:gap-8 md:pt-4 max-h-[75%] h-full`}>
        <div className="text-left order-2 text-sm" dangerouslySetInnerHTML={richText}></div>
        <div className={`order-3 self-center md:visible`} target="_blank">
          {
            image &&
            <img src={`${image.filename}/m/1600x0`} alt={image.alt}
              className="mx-auto w-auto object-cover max-h-24 md:max-h-[24rem] rounded-md md:neon" />
          }
        </div>
      </div>
    </ContentPage>

  )
};

export default ProjectTeaser;