"use client"

import usePaging from "../hooks/usePaging";

function ProgressBar() {
  const {
    totalHeight,
    scrollYPos,
    pageContentHeight
  } = usePaging()

  const progressBarHeight = Math.round(((scrollYPos / (totalHeight  - pageContentHeight )) * 100)) / 100


  return <>
    <div id="scroll" className="transition-all origin-top z-50 opacity-90 blur-md" style={{transform: `scaleY(${progressBarHeight})`}}>
    </div>
  </>
}

export default ProgressBar;