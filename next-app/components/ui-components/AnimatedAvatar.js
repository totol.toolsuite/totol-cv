"use client"
import { motion } from 'framer-motion'
import { useRef } from 'react'


function AnimatedAvatar({ src, isInView, className, isLittle = false }) {
  const ref = useRef(null)

  const imageVariants = {
    visible: { x: ['-100vw', '0vw'], scale: [1, 1.1, 0.9, 1] },
    before: { x: '-100vw' }
  }
  
  return <motion.div
    ref={ref}
    className={`${className} relative`}
    animate={isInView ? 'visible' : 'before'}
  >
    {isInView && <div className='w-full h-full bg-purple-500 blur-3xl absolute rounded-full opacity-30'></div>}
    <motion.div 
      variants={imageVariants}
      transition={{ease: 'easeInOut'}}
      animate={isInView ? 'visible' : 'before'}
    >
      <img
        className='z-30'
        src={src}
        width={isLittle ? 100 : 400}
        height={isLittle ? 100 : 400}
        alt="Avatar"
      />
    </motion.div>
  </motion.div>
}

export default AnimatedAvatar;