"use client"

import { TiArrowDownOutline } from "react-icons/ti";
import { TiArrowUpOutline } from "react-icons/ti";
import usePaging from "../hooks/usePaging";
import { useEffect } from "react";
import { animate, motion } from 'framer-motion'
import { createBreakpoint, useKeyPress, useKeyPressEvent } from "react-use";

function NavigationButtons() {
  const {
    currentPage,
    totalPages,
    scrollOnePageUp,
    scrollOnePageDown,
  } = usePaging()

  const useBreakpoint = createBreakpoint({ small: 400, medium: 600 });

  const isMedium = useBreakpoint() === 'medium'

  useEffect(() => {
    const event = addEventListener("keydown", function (e) {
      if (["ArrowUp", "ArrowDown"].indexOf(e.code) > -1) {
        e.preventDefault();
      }
    }, false);
    return () => removeEventListener("keydown", event)
  }, [])

  const [isPressedUp] = useKeyPress('ArrowUp');
  const [isPressedDown] = useKeyPress('ArrowDown');

  const scrollUpIfPossible = () => {
    if (currentPage !== 1) scrollOnePageUp()
  }

  const scrollDownIfPossible = () => {
    if (currentPage !== totalPages) scrollOnePageDown()
  }

  useKeyPressEvent('ArrowUp', scrollUpIfPossible);
  useKeyPressEvent('ArrowDown', scrollDownIfPossible);

  useEffect(() => {
    if (document) {
      const digit = document?.getElementById('animated-digit')
      if (digit) {
        animate(digit, { opacity: [1, 0] }, { 'duration': 0.2 })
        animate(digit, { y: [0, -30, 30, 0] }, { opacity: [1, 0, 0, 0, 0, 0, 1] }, { 'duration': 1 })
        animate(digit, { opacity: [0, 1] }, { 'duration': 0.2 })
      }
    }
  }, [currentPage])


  return <>
    <div className="fixed bottom-4 right-4 md:bottom-8 md:right-8 flex flex-col gap-8">
      <button 
        disabled={!currentPage || currentPage === 1}
        onClick={scrollUpIfPossible} 
        className={`opacity-75 transition-all  
          ${isPressedUp && 'translate-y-3 opacity-100'}
          ${currentPage === 1 ? 'opacity-30 cursor-not-allowed' : 'hover:scale-105 hover:-translate-y-1 active:translate-y-1'}
        `}
      >
        <TiArrowUpOutline size={isMedium ? 72 : 48}></TiArrowUpOutline>
      </button>
      <button 
        disabled={currentPage === totalPages}
        onClick={scrollDownIfPossible} 
        className={`opacity-75 transition-all
        ${isPressedDown && 'translate-y-3 opacity-100'}
        ${currentPage === totalPages ? 'opacity-30 cursor-not-allowed' : 'hover:scale-105 hover:-translate-y-1 active:translate-y-1'}
      `}><TiArrowDownOutline size={isMedium ? 72 : 48}></TiArrowDownOutline></button>
    </div>
    {
      currentPage && totalPages
        ? <div >
        <div className="fixed bottom-4 left-4 md:bottom-8 md:left-8">
          <div className="text-2xl md:text-4xl flex proportional-nums font-header">
            <motion.div
              id="animated-digit"
            >{currentPage}</motion.div>
          </div>
        </div>
        <div className="fixed bottom-4 left-12 md:bottom-8 md:left-20">
          <div className="text-2xl md:text-4xl flex proportional-nums font-header">
            / {totalPages}
          </div>
        </div>
        </div>
        : ''
    }
  </>
}

export default NavigationButtons;