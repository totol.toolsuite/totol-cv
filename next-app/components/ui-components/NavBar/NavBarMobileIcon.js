"use client";

function NavBarMobileIcon({ icon, link }) {
  return (
    <a
      href={link}
      target="_blank"
      className="h-full w-full md:h-1/5 hover:-translate-y-1 active:translate-y-1 transition-all flex justify-center"
    >
      <img src={icon} className="h-full aspect-square rounded-lg"></img>
    </a>
  );
}
export default NavBarMobileIcon;
