"use client"

import { motion } from 'framer-motion'
import { useRef } from 'react'
import { createBreakpoint, useHoverDirty } from 'react-use'
function NavBarItem({ items, completedCount, handleClick, children }) {
  const isSelected = completedCount / items.length > 0 && completedCount / items.length < 1
  const isCompleted = completedCount >= items.length

  const element = useRef(null)
  const isHovering = useHoverDirty(element);

  const scaleRatio = 1 + (completedCount / items.length / 4)
  const completeScaleSequence = [
    1.8, 0.9, 1
  ]

  const svgSize = 220
  const circleSize = 110
  const circleR = 70

  return <motion.div
    className={`min-w-[150px] text-white`}
    animate={{
      scale: isCompleted ? completeScaleSequence : scaleRatio,
    }}
  >
    <a ref={element} onClick={handleClick} className='flex items-center justify-center relative hover:scale-105 hover:-translate-y-1 active:translate-y-1 transition-all hover:no-underline'>
      <svg
        className='absolute hover:cursor-pointer [&>circle]:transition-all '
        width={svgSize} height={svgSize} viewBox={`0 0 ${svgSize} ${svgSize}`} >
        <defs>
          <filter id="shadow">
            <feDropShadow dx="-0.5" dy="-0.5" stdDeviation="1" floodColor="#FFFFFF" />
            <feDropShadow dx="-0.5" dy="-0.5" stdDeviation="2" floodColor="#ffc000" />
            <feDropShadow dx="-0.5" dy="-0.5" stdDeviation="4" floodColor="#ffc000" />
          </filter>
          <filter id="light">
            <feDropShadow dx="-0.5" dy="-0.5" stdDeviation="1" floodColor="#FFFFFF" />
            <feDropShadow dx="-0.5" dy="-0.5" stdDeviation="2" floodColor="#E384FF" />
            <feDropShadow dx="-0.5" dy="-0.5" stdDeviation="4" floodColor="#865DFF" />
          </filter>
        </defs>
        <motion.circle
          filter={isHovering ? "url(#light)" : isSelected ? "url(#shadow)" : ''}
          cx={circleSize} cy={circleSize} r={circleR} pathLength="100" stroke="#FFFFFF" opacity={1} fill="transparent" strokeWidth="1%"
          strokeDasharray={100}
          strokeDashoffset={-(100 - (completedCount / items.length * 100)) || 1}
          transition={{ ease: 'linear' }}
        />
      </svg>
      <motion.span
        className={`text-xl transition-colors font-header ${isHovering ? 'text-pink-300' : isSelected ? 'text-gold' : 'text-white'}`}>
        {children}
      </motion.span>

    </a>
  </motion.div>
}
export default NavBarItem