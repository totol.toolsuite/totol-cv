"use client";

import usePaging from "@/components/hooks/usePaging";
import { useStories } from "@/components/hooks/useStories";
import { motion } from "framer-motion";
import dynamic from "next/dynamic";
import { useEffect } from "react";
import { createBreakpoint } from "react-use";
import NavBarMobileIcon from "./NavBarMobileIcon";

const NavigationButtons = dynamic(() => import("../NavigationButtons"), {
  ssr: false,
});
const NavBarItem = dynamic(() => import("./NavBarItem"), { ssr: false });

function NavBar() {
  const { projects, experiences, formations } = useStories();

  const useBreakpoint = createBreakpoint({ small: 400, medium: 600 });
  const isMedium = useBreakpoint() === "medium";

  useEffect(() => {
    scrollTo({
      top: 0,
      behavior: "instant",
    });
  }, []);

  const { totalPageCompleted, goToPage } = usePaging();

  const whoAmICompleted = Math.min(totalPageCompleted, 1);
  const projectsCompleted = Math.min(
    totalPageCompleted - whoAmICompleted,
    projects.length
  );
  const experiencesCompleted = Math.min(
    totalPageCompleted - whoAmICompleted - projectsCompleted,
    experiences.length
  );
  const formationsCompleted = Math.min(
    totalPageCompleted -
      whoAmICompleted -
      experiencesCompleted -
      projectsCompleted,
    formations.length
  );

  const whoAmIPage = 1;
  const projectPage = 1 + 2;
  const experiencePage = 1 + 2 + projects.length;
  const formationsPage = 1 + 2 + projects.length + experiences.length;

  return (
    <nav
      id="nav"
      className="fixed bottom-0 md:top-0 z-50 w-full h-[10dvh] md:h-[25dvh] bg-dark-500"
    >
      <motion.div
        animate={{ opacity: 1 }}
        initial={{ opacity: 0 }}
        transition={{ duration: 2 }}
        className="hidden md:block h-full"
      >
        <NavigationButtons></NavigationButtons>
        <div
          id="navbar"
          className="flex justify-evenly items-center w-full h-full px-12 bg-stars neon bg-top"
        >
          <NavBarItem
            items={[1]}
            completedCount={whoAmICompleted}
            handleClick={() => goToPage(whoAmIPage)}
          >
            Qui suis-je
          </NavBarItem>
          <NavBarItem
            items={projects}
            completedCount={projectsCompleted}
            handleClick={() => goToPage(projectPage)}
          >
            Projets
          </NavBarItem>
          <NavBarItem
            items={experiences}
            completedCount={experiencesCompleted}
            handleClick={() => goToPage(experiencePage)}
          >
            Exp pro
          </NavBarItem>
          <NavBarItem
            items={formations}
            completedCount={formationsCompleted}
            handleClick={() => goToPage(formationsPage)}
          >
            Formations
          </NavBarItem>
          <div className="absolute right-8 flex flex-col justify-center items-stretch gap-2 h-full">
            <NavBarMobileIcon
              link="https://www.linkedin.com/in/anatole-acqueberge-a2baa1136/"
              icon="/images/linkedin-logo.png"
            ></NavBarMobileIcon>
            <NavBarMobileIcon
              link="https://www.malt.fr/profile/anatoleacqueberge"
              icon="/images/malt-logo.png"
            ></NavBarMobileIcon>
            <NavBarMobileIcon
              link="https://gitlab.com/totol.toolsuite"
              icon="/images/gitlab-logo.png"
            ></NavBarMobileIcon>
            <NavBarMobileIcon
              link="https://www.codingame.com/profile/963e5aedacd92de95a47d9ef7372b5ef8683343"
              icon="/images/codingame-logo.png"
            ></NavBarMobileIcon>
          </div>
        </div>
      </motion.div>
      <motion.div
        animate={{ y: 0 }}
        initial={{ y: 300 }}
        transition={{ duration: 1.5 }}
        className="md:hidden flex items-center justify-between h-full p-3 neon"
      >
        <NavBarMobileIcon
          link="https://www.linkedin.com/in/anatole-acqueberge-a2baa1136/"
          icon="/images/linkedin-logo.png"
        ></NavBarMobileIcon>
        <NavBarMobileIcon
          link="https://www.malt.fr/profile/anatoleacqueberge"
          icon="/images/malt-logo.png"
        ></NavBarMobileIcon>
        <NavBarMobileIcon
          link="https://gitlab.com/totol.toolsuite"
          icon="/images/gitlab-logo.png"
        ></NavBarMobileIcon>
        <NavBarMobileIcon
          link="https://www.codingame.com/profile/963e5aedacd92de95a47d9ef7372b5ef8683343"
          icon="/images/codingame-logo.png"
        ></NavBarMobileIcon>
      </motion.div>
    </nav>
  );
}
export default NavBar;
