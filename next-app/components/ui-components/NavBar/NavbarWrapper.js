import NavBar from "./NavBar";
async function NavBarWrapper() {
  return  <NavBar 
    formations={formations.data.stories} 
    projects={projects.data.stories} 
    experiences={experiences.data.stories}
  ></NavBar>
}
export default NavBarWrapper;
