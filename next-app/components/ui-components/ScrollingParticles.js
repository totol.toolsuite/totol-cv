"use client"

import { useEffect, useState } from "react";
import { motion } from "framer-motion"
import useDebounceScroll from "../hooks/useDebouncedScroll";
function ScrollingParticles() {

  const [oldScroll, setOldScroll] = useState(0)

  const [isScrollingDown, setIsScrollingDown] = useState(false)
  const [isScrollingUp, setIsScrollingUp] = useState(false)

  const scrollPos = useDebounceScroll()
  
  useEffect(() => {
    setIsScrollingDown(false)
    setIsScrollingUp(false)
    let timeout
    if (scrollPos > oldScroll) {
      setIsScrollingDown(true)
      timeout = setTimeout(() => setIsScrollingDown(false), 200)
    }

    if (scrollPos < oldScroll) {
      setIsScrollingUp(true)
      timeout = setTimeout(() => setIsScrollingUp(false), 200)
    }

    setOldScroll(scrollPos)
    return () => {
      clearTimeout(timeout)
    }
  }, [scrollPos])

  const particle1Class = `fixed particle z-20 particle-1 ${isScrollingDown && ' particle-1--scrollup'} ${isScrollingUp && ' particle-1--scrolldown'}`
  const particle2Class = `fixed particle z-20 particle-2 ${isScrollingDown && ' particle-2--scrollup'} ${isScrollingUp && ' particle-2--scrolldown'}`
  const particle3Class = `fixed particle z-20 particle-3 ${isScrollingDown && ' particle-3--scrollup'} ${isScrollingUp && ' particle-3--scrolldown'}`
  const particle4Class = `fixed particle z-20 particle-4 ${isScrollingDown && ' particle-4--scrollup'} ${isScrollingUp && ' particle-4--scrolldown'}`
  const particlefixedClass = `fixed particle z-20 fixed-particles`

  return (
    <>
      <div className={particle1Class}></div>
      <div className={particle2Class}></div>
      <div className={particle3Class}></div>
      <div className={particle4Class}></div>
      {!isScrollingDown && !isScrollingUp && <>
          <motion.div animate={{ opacity: 1 }} initial={{ opacity: 0 }} transition={{ duration: 5 }} className={particlefixedClass}></motion.div>
          <div className={particlefixedClass}></div>
        </>}
    </>
  );
}

export default ScrollingParticles;