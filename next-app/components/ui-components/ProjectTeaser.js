"use client"

import { renderRichText } from "@storyblok/react";
import ContentPage from "./ContentPage";
import { useInView } from "framer-motion"
import { useRef } from "react";
import { motion } from 'framer-motion'
const ProjectTeaser = ({ title, image, url, description, date, technos, imageLeft, shortDescription }) => {
  const richText = { __html: renderRichText(description) };

  const ref = useRef(null)
  const isInView = useInView(ref)

  const titleVariants = {
    visible: { y: 0 },
    before: { y: 100 }
  }

  return (
    <ContentPage >
      <div className="max-h-[25%]" ref={ref}>
        <div className="overflow-hidden">
          <div className="float-left neon p-4 top-4 left-4 md:top-12 md:left-36 text-left font-bold md:text-xl md:neon">{new Date(date).getFullYear()}</div>
          <motion.h1 
            className="text-3xl md:text-5xl ml-4"
            variants={titleVariants}
            animate={isInView ? 'visible' : 'before'}
            transition={{ duration: 0.7 }}
          >
            {title}
          </motion.h1>
        </div>
        
        <a target="_blank" href={url} className="italic"><div>{url}</div></a>
        <div>{technos.join(' • ')}</div>
      </div>
      <div className={`grid ${imageLeft ? 'md:grid-cols-[3fr_7fr]' : 'md:grid-cols-[7fr_3fr]'} items-center justify-center gap-2 md:gap-8 md:pt-4 max-h-[75%] h-full`}>
        <div className="text-left order-2 text-sm" dangerouslySetInnerHTML={richText}></div>
        <a className={`${imageLeft ? 'order-1' : 'order-3'} self-center md:visible`} href={url} target="_blank">
          {
            image &&
            <img src={`${image.filename}/m/1600x0`} alt={image.alt}
              className="mx-auto w-auto object-cover min-h-[15rem] max-h-32 md:max-h-[24rem] rounded-md md:neon" />
          }
        </a>
      </div>
    </ContentPage>

  )
};

export default ProjectTeaser;