function ContentPage({children}) {
  return <div className="min-h-[90dvh] md:min-h-[75dvh] md:h-[75dvh] p-4 md:p-8 overflow-hidden sm:px-12 md:px-20 lg:px-36 relative">
    <div className="w-full h-full shadow-xl shadow-purple-500 absolute left-0 top-0 blur-xl bg-purple-500 opacity-5"></div>
    {children}
  </div>
}

export default ContentPage;