"use client"

import { useRef } from "react";
import useCursorLight from "../hooks/useCursorLight";

function CursorLight() {
  const ref = useRef(null)
  useCursorLight(ref)

  return <span className="absolute bg-purple-500 h-72 w-72 -translate-x-36 -translate-y-36 opacity-30 rounded-full blur-3xl" ref={ref}></span>
}

export default CursorLight;