"use client"

import { motion, useInView } from "framer-motion"
import { useRef } from "react"

function AnimatedText({ className, children, stagger = 0.05 }) {

  const ref = useRef(null)
  const isInView = useInView(ref)

  const letterVariants = {
    visible: { y: 0, opacity: 1 },
    before: { y: 50, opacity: 0 }
  }

  const name = children.split('').map(x => {
    if (x === ' ') return '\u00A0'
    return x
  })

  return (<motion.div
    ref={ref}
    className={`flex ${className}`}
    variants={letterVariants}
    animate={isInView ? 'visible' : 'before'}
    transition={{ staggerChildren: stagger }}
  >{name.map((x, i) => <motion.div variants={letterVariants} transition={{ duration: .2 }} key={i}>{x}</motion.div>)}</motion.div>);
}

export default AnimatedText;