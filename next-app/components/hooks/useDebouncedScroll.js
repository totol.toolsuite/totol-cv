import { useEffect, useState } from 'react';
import throttle from 'lodash/throttle';

const useDebouncedScroll = (delay = 50) => {

  const [scrollY, setScrollY] = useState(0);

  useEffect(() => {
    // Create a debounced version of the setScrollY function
    const debouncedSetScrollY = throttle(() => setScrollY(window.scrollY), delay);

    // Update the vertical scroll position when the window is scrolled
    const handleScroll = () => {
      debouncedSetScrollY();
    };

    // Attach the scroll event listener
    window.addEventListener('scroll', handleScroll);

    // Cleanup: remove the event listener on component unmount
    return () => {
      window.removeEventListener('scroll', handleScroll);
      // Ensure any remaining debounced function calls are executed before unmounting
      debouncedSetScrollY.flush();
    };
  }, [delay]);

  return scrollY;
};

export default useDebouncedScroll