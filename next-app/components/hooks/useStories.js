"use client"

import { getStoryblokApi } from "@storyblok/react";
import { createContext, useContext, useEffect, useState } from "react";

const StoriesContext = createContext({
  projects: [],
  experiences: [],
  formations: []
})

export function useStories() {
  const stories = useContext(StoriesContext)
  return {
    projects: stories.projects,
    setProjects: stories.setProjects,
    experiences: stories.experiences,
    setExperiences: stories.setExperiences,
    formations: stories.formations,
    setFormations: stories.setFormations,
  }
}

export default function StoriesContextProvider ({children}) {
  const [projects, setProjects] = useState([])
  const [experiences, setExperiences] = useState([])
  const [formations, setFormations] = useState([])

  const storyblokApi = getStoryblokApi();

  const sbFetch = (type) => storyblokApi.get(`cdn/stories`, {
    version: "draft",
    starts_with: `${type}/`,
    is_startpage: false
  });

  useEffect(() => {
    Promise.all([
      sbFetch('projects'),
      sbFetch('professionnal-experiences'),
      sbFetch('formations'),
    ]).then(([pr, ex, fo]) => {
      try {
        setProjects(pr.data.stories)
        setExperiences(ex.data.stories)
        setFormations(fo.data.stories)
      } catch (e) {
        setProjects([])
        setExperiences([])
        setFormations([])
      }
    })
  }, [])

  return <StoriesContext.Provider value={{
    projects, setProjects,  experiences, setExperiences, formations, setFormations
  }}>
    {children}
  </StoriesContext.Provider>
}