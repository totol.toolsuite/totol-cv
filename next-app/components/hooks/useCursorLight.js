"use client";

import { useMouse } from "react-use";

const useCursorLight = (ref) => {
  const { docX, docY } = useMouse(ref);

  let navbarHeight = 0;

  try {
    if (typeof window !== "undefined") {
      const navbar = document.getElementById("navbar");
      if (navbar) {
        navbarHeight = navbar.clientHeight;
      }
    }
  } catch (e) {
    console.log("e", e);
  }

  if (ref.current) {
    ref.current.style.left = `${docX}px`;
    ref.current.style.top = `${docY - navbarHeight}px`;
  }
};

export default useCursorLight;
