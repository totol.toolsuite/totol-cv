"use client"

import { useEffect, useMemo, useState } from "react";
import { createBreakpoint, useWindowSize } from "react-use";
import { useStories } from "./useStories";
import useDebounceScroll from "./useDebouncedScroll";

export default function usePaging() {
  const { height: screenHeight } = useWindowSize();
  const [navHeight, setNavHeight] = useState(0)
  const pageContentHeight = screenHeight - navHeight
  const useBreakpoint = createBreakpoint({ small: 400, medium: 600 });
  const isMedium = useBreakpoint() === 'medium'

  const debouncedScroll = useDebounceScroll(isMedium ? 50 : 400)
  const scrollYPos = Math.floor(debouncedScroll)

  const [totalHeight, setTotalHeight] = useState(0)

  const totalPageCompleted = useMemo(() => scrollYPos / pageContentHeight, [scrollYPos, pageContentHeight])
  
  const [totalPages, setTotalPages] = useState(2)

  const currentPage = useMemo(() => Math.min(Math.ceil(scrollYPos / pageContentHeight) + 1, totalPages), [scrollYPos, pageContentHeight]) 

  const goToPage = (pageNumber) => {
    scrollTo({
      top: (pageNumber -1) * pageContentHeight,
      behavior: "smooth",
    })
  }

  const scrollOnePageUp = () => {
    scrollTo({
      top: ((currentPage -2) * pageContentHeight - 1),
      behavior: "smooth",
    })
  }
  
  const scrollOnePageDown = () => {
    scrollTo({
      top: ((currentPage) * pageContentHeight),
      behavior: "smooth",
    })
  }

  useEffect(() => {
    if (document) {
      setNavHeight(document?.getElementById('nav')?.offsetHeight)
      setTotalHeight(document?.body?.offsetHeight)
    }
  }, [scrollYPos])
   

  const { projects, experiences, formations } = useStories()
  useEffect(() => {
    const total = 2 + projects.length + experiences.length + formations.length
    if (total > 2 && total !== totalPages) {
      setTotalPages(total)
    }
  }, [projects, experiences, formations])

  return {
    screenHeight,
    navHeight,
    pageContentHeight,
    scrollYPos,
    totalHeight,
    totalPageCompleted,
    currentPage,
    totalPages,
    setTotalPages,
    scrollOnePageUp,
    scrollOnePageDown,
    goToPage,
    setTotalHeight,
  }
}
