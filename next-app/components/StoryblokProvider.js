/** 1. Tag it as client component */
"use client";
import { storyblokInit, apiPlugin } from "@storyblok/react/rsc";

/** 2. Import your components */
import Page from "./storyblok-components/Page";
import Teaser from "./storyblok-components/Teaser";
import Grid from "./storyblok-components/Grid";
import Feature from "./storyblok-components/Feature";
import Project from "./storyblok-components/Project";
import FormationsList from "./storyblok-components/FormationsList";
import ProjectsList from "./storyblok-components/ProjectsList";
import ExperiencesList from "./storyblok-components/ExperiencesList";
import About from "./storyblok-components/About";
import Presentation from "./storyblok-components/Presentation";

/** 3. Initialize it as usual */
storyblokInit({
  accessToken: process.env.storyblokPreviewToken,
  use: [apiPlugin],
  components: {
    teaser: Teaser,
    page: Page,
    grid: Grid,
    feature: Feature,
    project: Project,
    projects_list: ProjectsList,
    formations_list: FormationsList,
    experiences_list: ExperiencesList,
    presentation: Presentation,
    about: About
  },
});

export default function StoryblokProvider({ children }) {
  return children;
}
