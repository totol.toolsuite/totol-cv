"use client"
import { useRef } from "react";
import AnimatedAvatar from "../ui-components/AnimatedAvatar";
import AnimatedText from "../ui-components/AnimatedText";
import ContentPage from "../ui-components/ContentPage";
import usePaging from "../hooks/usePaging";
import { createBreakpoint } from "react-use";
import Portal from "../ui-components/Portal";

function About({blok}) {
  const avatar = useRef(null)
  const useBreakpoint = createBreakpoint({ small: 400, medium: 600 });
  const isMedium = useBreakpoint() === 'medium'

  const {
    totalPageCompleted
  } = usePaging()

  const isInView = totalPageCompleted < 0.6

  const handleClick = () => {
    scrollTo({
      top: 0,
      behavior: "smooth",
    })
  }

  return <ContentPage>
    <div ref={avatar} className="flex flex-col justify-start items-center h-full">
      <AnimatedText className='text-5xl sm:text-6xl lg:text-7xl xl:text-8xl font-bold font-header'>{blok.title.split(' ')[0]}</AnimatedText>
      <AnimatedText className='text-5xl sm:text-6xl lg:text-7xl xl:text-8xl font-bold font-header'>{blok.title.split(' ')[1]}</AnimatedText>
      <AnimatedAvatar src={`${blok.image.filename}/m/1600x0`} isInView={isInView}></AnimatedAvatar>
      {isMedium && <Portal selector={'#navbar'}><a className={'absolute h-full flex items-center left-8 cursor-pointer'} onClick={handleClick}><AnimatedAvatar src={`${blok.image.filename}/m/1600x0`} isInView={!isInView} isLittle ></AnimatedAvatar></a></Portal>}
    </div>
  </ContentPage>
}

export default About;