"use client"

import ProjectTeaser from "../ui-components/ProjectTeaser";
import { useStories } from "../hooks/useStories";

export default function ProjectsList() {
  const { projects } = useStories()
  return <div >
      {
        projects.map((project, i) => (
          <ProjectTeaser key={project.id} {...project.content}></ProjectTeaser>
        ))
      }
  </div>
}