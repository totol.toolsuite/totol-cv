"use client"
import FormationTeaser from "../ui-components/FormationTeaser";
import { useStories } from "../hooks/useStories";

export default function FormationsList() {
  const { formations } = useStories()

  return <div>
      {
        formations.map((fo, i) => (
          <FormationTeaser key={fo.id} {...fo.content}></FormationTeaser>
        ))
      }
  </div>
}