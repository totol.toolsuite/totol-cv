"use client"

import ExperienceTeaser from "../ui-components/ExperienceTeaser";
import { useStories } from "../hooks/useStories";

export default function ExperiencesList() {
  const { experiences } = useStories()

  return <div>
      {
        experiences.map((ex, i) => (
          <ExperienceTeaser key={ex.id} {...ex.content}></ExperienceTeaser>
        ))
      }
  </div>
}