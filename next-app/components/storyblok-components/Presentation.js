"use client"

import ContentPage from "../ui-components/ContentPage";
import { renderRichText } from "@storyblok/react";
import { motion, useInView } from "framer-motion"
import { useRef } from "react"

function Presentation({ blok }) {
  const richText = { __html: renderRichText(blok.text) };

  const ref = useRef(null)
  const isInView = useInView(ref)

  const letterVariants = {
    visible: { y: 0, opacity: 1 },
    before: { y: 50, opacity: 0 }
  }

  return <ContentPage>
    <div className="neon py-12 mb-12 hidden md:block">
      <h2 className="mb-4 md:text-3xl">Moi en quelques pictogrammes</h2>
      <motion.div className="flex flex-wrap gap-8 justify-center mb-8 overflow-hidden"
        ref={ref}
        variants={letterVariants}
        animate={isInView ? 'visible' : 'before'}
        transition={{ staggerChildren: 0.08 }}>
        {blok.pictos.map(x => <motion.img key={x.id} src={`${x.filename}/m/1600x0`} alt={x.alt} className="h-[58px] md:h-16 w-[58px] md:w-16" variants={letterVariants} transition={{ duration: .2 }}/>)}
      </motion.div>
    </div>
    <h2 className="text-3xl">Qui suis-je ? </h2>
    <div className="text-left text-sm" dangerouslySetInnerHTML={richText}></div>
  </ContentPage>
}

export default Presentation;
