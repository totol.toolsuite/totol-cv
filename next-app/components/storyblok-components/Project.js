import { storyblokEditable, StoryblokComponent } from "@storyblok/react/rsc";

const Project = ({ blok }) => {
  return <div {...storyblokEditable(blok)} className="text-center">
    <h1>{blok.title}</h1>
    <h2>{JSON.stringify(blok.description)}</h2>
  </div>
}
;

export default Project;