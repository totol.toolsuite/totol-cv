import StoriesContextProvider from "@/components/hooks/useStories";
import NavBar from "@/components/ui-components/NavBar/NavBar";
import ProgressBar from "@/components/ui-components/ProgressBar";
import ScrollingParticles from "@/components/ui-components/ScrollingParticles";
import {
  getStoryblokApi,
} from "@storyblok/react/rsc";
import StoryblokStory from "@storyblok/react/story";

export default async function Home({ params }) {
  const storyblokApi = getStoryblokApi();
  const sbParams = { version: "draft" };

  let res
  try {
    res = await storyblokApi.get(`cdn/stories/home`, sbParams);
  } catch (e) {
    console.log(e)
  }
  

  return (
    <div className="h-full">
      <StoriesContextProvider>
        <NavBar></NavBar>
        <ProgressBar></ProgressBar>
        <ScrollingParticles></ScrollingParticles>
        {
          res
            ? <StoryblokStory story={res.data.story} />
            : 'Problème de récup des données'
        }
      </StoriesContextProvider>
    </div>
  );
}