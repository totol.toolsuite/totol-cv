import StoryblokProvider from "@/components/StoryblokProvider";
import CursorLight from "@/components/ui-components/CursorLight";
import { GoogleAnalytics } from "@next/third-parties/google";
import { apiPlugin, storyblokInit } from "@storyblok/react/rsc";
import { Asul, Orbitron } from "next/font/google";
import "./globals.scss";

const asul = Asul({
  subsets: ["latin"],
  weight: ["400", "700"],
  variable: "--font-asul",
});

const orbitron = Orbitron({
  subsets: ["latin"],
  weight: ["400"],
  variable: "--font-orbitron",
});

storyblokInit({
  accessToken: process.env.storyblokPreviewToken,
  use: [apiPlugin],
});

export const metadata = {
  title: "Anatole Acqueberge • Expert Frontend Freelance",
};

export default function RootLayout({ children }) {
  return (
    <StoryblokProvider>
      <html lang="fr">
        <body
          className={`min-h-screen relative mb-[10dvh] md:mt-[25dvh] md:mb-0 ${asul.variable} ${orbitron.variable}`}
        >
          <CursorLight></CursorLight>
          {children}
        </body>
        <GoogleAnalytics gaId="G-LR49EY4W3F" />
      </html>
    </StoryblokProvider>
  );
}
