/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    colors: {
      "dark": {
        400: '#222031',
        500: '#191825',
        600: '#111019',
      },
      "purple": {
        500: '#865DFF',
      },
      "pink": {
        500: '#E384FF',
        300: '#FFA3FD'
      },
      "white": '#FFFFFF',
      "black": '#000000',
      "gold": '#ffc000'
    },
    extend: {
      fontFamily: {
        header: ['var(--font-orbitron)'],
        body: ['var(--font-asul)'],
      },
      backgroundImage: {
        'stars': "url('/images/bg-stars.png')",
      }
    }
  },
  plugins: [],
}
