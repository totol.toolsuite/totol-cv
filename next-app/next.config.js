/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    storyblokPreviewToken: process.env.STORYBLOK_PREVIEW_TOKEN
  }
}

module.exports = nextConfig
