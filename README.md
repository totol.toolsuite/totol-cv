# Totol CV

## Technos
- Next.js (curr 14)
- React (curr 18)
- Storyblok (backoffice)
- Tailwind (css)
- Framer Motion (anims)


## Why
Simple project, done to show some projects, and explore few animations

## Local start
```
  cd next-app
  npm install
  npm run dev
```
